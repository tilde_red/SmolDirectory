The links here go directly to the RSS or Atom feeds supplied by the site.

## Blogs

* [Alex Molas' BLog](https://www.alexmolas.com/feed.xml)
* [Causal Agency Blog](https://text.causal.agency/feed.atom)
* [Corey Reichle's Blog](https://homepages.buffalo.edu/~creichle/index.xml)
* [Nora's Blog](https://nora.codes/index.xml)
* [Ploum's Blog](https://ploum.net/atom_en.xml)
* [Pluralistic](https://pluralistic.net/feed/)
* [Solene's Blog](https://dataswamp.org/~solene/rss.xml)

## News

* https://fivethirtyeight.com/all/feed
* https://theintercept.com/feed/?lang=en
* https://www.dsausa.org/rss2
* https://www.theregister.com/headlines.atom
* https://rss.dw.com/rdf/rss-en-all
* https://www.propublica.org/feeds/propublica/main
* https://www.sciencenews.org/feed
* https://www.sciencedaily.com/rss/top/technology.xml
* https://www.sciencedaily.com/rss/top.xml
* https://www.sciencedaily.com/rss/most_popular.xml
* https://www.democracynow.org/democracynow.rss
