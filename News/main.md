There are going to be some sites here, that are not explicitly anti-facist, or anti-racist.  These are here for a more complete view of current events, but must still adhere to the Small Web idea.

* [CNN Lite Page](https://lite.cnn.com)
* [CS Monitor](https://www.csmonitor.com/layout/set/text/textedition)
* [CrimeThinc](https://crimethinc.com/)
* [LibCom](https://libcom.org/)
* [NPR](https://text.npr.org)
* [PBS](https://lite.pbs.org/)
* [WTTR](https://wttr.in)