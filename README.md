# SmolDirectory

A directory of sites that support:

* A smaller view of the internet
* A curated directory of links
* Anti-racism
* Anti-fascism

Some of these requirements can be flexible (We're not dogmatic).  For example, while there are few news sites that cover things from a non-capitalist POV, needing to access that news is still critical (See: No ethical consumption under capitalism).  So, you'll find CNN there, NPR, etc.

We should, however, flag these for what they are, as well.

## Organization

A filesystem is already a pretty good hierarchical store for documents.  And, while there are many complaints about MarkDown, it's one of the most easily accesible, and convertible formats for simple text documents.

So, perusing the repo, you should already be able to see how to organize the lists, and how to update the lists, to include creating additional categories (Directories in the filesystem).  Lists should be titled "main.md".

## Display and interaction

There's no code behind displaying and navigating the lists yet.  That's coming, and will be in a separate repo.